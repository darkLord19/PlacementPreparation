### Stable Sort
If values of keys are same then they appear in same order in sorted list as they were in original list.

In place sorting algorithm takes constant amount of extra memory.


## Selection Sort
Choose smallest element from list and swap with 1st position. Then choose 2nd smallest and swap with second position.

3 5 1 2 4 => 1 5 3 2 4 => 1 2 3 5 4 => 1 2 3 4 5

Running Time Complexity = O(n^2)
Extra Space Complexity = O(1)


## Bubble Sort
Bubble out the maximum number at the end of the list

Running Time Complexity = O(n^2)
Extra Space Complexity = O(1)

## Insertion Sort

Running Time Complexity = O(n^2)
Extra Space Complexity = O(1)

## Merge Sort
It is stable sort Algorithm.

Running Time Complexity = O(nlogn)
Extra Space Complexity = O(n)