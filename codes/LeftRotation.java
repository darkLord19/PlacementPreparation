public class LeftRotation{
	static int gcd(int a, int b){
		if(a==0)
			return b;
		return gcd(b%a, a);
	}
	public static void main(String[] args) {
		int[] arr = {1,2,3,4,5,6,};
		int g = gcd(6, 3);
		for(int i=0;i<g;i++){
			int tmp = arr[i];
			int j = i;
			int k = 0;
			while(true){
				k = (j+3)%6;
				if(k==i)
					break;
				arr[j] = arr[k];
				j = k;
			}
			arr[j] = tmp;
		}
		for(int i=0;i<6;i++)
			System.out.print(arr[i]+" ");
		System.out.println();
	}
}