## Eucledian Algorithm to find GCD
GCD of two numbers doesn’t change if smaller number is subtracted from a bigger number.

```java
if(a>b)
	gcd(a,b) = gcd(a-b, b);
else
	gcd(a, b) = gcd(a, b-a);
